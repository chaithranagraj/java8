package com.example.java8.optional;

import java.util.Optional;

/**
 * @author chaithraNagraj on 03-04-2022
 */
public class Example {
    public static void main(String[] args) {
        String[] str = new String[10];
        Optional<String> checkNull = Optional.ofNullable(str[5]);
        if (checkNull.isPresent()) {
            String lowercaseSting = str[5].toLowerCase();
            System.out.println(lowercaseSting);
        } else {
            System.out.println("String value is not present");
        }
    }
}
