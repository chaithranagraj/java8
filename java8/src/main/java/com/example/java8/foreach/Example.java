package com.example.java8.foreach;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chaithraNagraj on 03-04-2022
 */
public class Example {
    public static void main(String[]args){
        List<String> lipsticsList =new ArrayList<>();
        lipsticsList.add("Mac");
        lipsticsList.add("Maybelline");
        lipsticsList.add("ReneeFab");
        lipsticsList.add("Sugar");
        lipsticsList.add("Sophora");
        lipsticsList.forEach(
                lipstic ->System.out.println(lipstic)
        );
    }
}
