package com.example.java8.lamdaexpresions;

/**
 * @author chaithraNagraj on 03-04-2022
 */
public interface Draw {

    public void draw();

}
