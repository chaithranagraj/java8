package com.example.java8.lamdaexpresions;

/**
 * @author chaithraNagraj on 03-04-2022
 */
public class Example1 {


    public static void main(String[] args) {
        int width = 10;

        Draw drawable = () -> {
            System.out.println("Width : " + width);
        };
        drawable.draw();
    }
}

