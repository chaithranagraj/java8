package com.example.java8.collectors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author chaithraNagraj on 05-04-2022
 */

public class Example {

    public static void main(String[] args) {
        List<Product> productList = new ArrayList<>();

        productList.add(new Product(1, "top", 435));
        productList.add(new Product(1, "bottom", 435));
        productList.add(new Product(1, "kurthi", 435));
        productList.add(new Product(1, "shorts", 53635));
        productList.add(new Product(1, "chudi", 4375));
        productList.add(new Product(1, "saree", 4225));
        productList.add(new Product(1, "dresses", 335));
        productList.add(new Product(1, "jumsuits", 1235));

        List<Float> priceList = productList.stream().map(product ->
                product.price).collect(Collectors.toList());//collecting as list

        Set<String> productNameList = productList.stream().map(product ->
                product.name).collect(Collectors.toSet()); //collecting as list

        System.out.println(priceList);
        System.out.println(productNameList);
    }
}
