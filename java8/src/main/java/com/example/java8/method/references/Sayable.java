package com.example.java8.method.references;

/**
 * @author chaithraNagraj on 03-04-2022
 */
interface Sayable {
    void say();
}
